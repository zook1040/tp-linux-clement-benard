# TP2 : 

A faire sur les 3 serv ! 

```bash
$ sudo nano /etc/hosts
127.0.0.0.1     db  db.tp.cesi
192.168.199.30  web web.tp.cesi
192.168.199.20  rp rp.tp.cesi

$ sudo nano /etc/hostname
db.tp.cesi
```

# I. Base de données

```bash
$ sudo yum install mariadb-server -y
Installed:
  mariadb-server.x86_64 1:5.5.68-1.el7
$ sudo systemctl enable mariadb --now
Created symlink from /etc/systemd/system/multi-user.target.wants/mariadb.service to /usr/lib/systemd/system/mariadb.service.
```
Je rentre dans la base donnée pour la configuration
```bash
$ sudo mysql_secure_installation
MariaDB [(none)]>  ... Success!
$ use mysql
$ create database baseclem
$ show databases
MariaDB [mysql]> show databases
    -> ;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| baseclem           |
| mysql              |
| performance_schema |
+--------------------+
```

```bash
$ grant all on baseclem.* to baseclembob@localhost
   - > identified by 'recom33';
Query OK, 0 rows affected (0.00 sec)

$ sudo firewall-cmd --add-port=3306/tcp --permanent
$ sudo firewall-cmd --reload
$ sudo firewall-cmd --list-all
 ports: 3306/tcp
```

# II. Serveur Web

```bash
$ sudo yum install httpd
Installed:
  httpd.x86_64 0:2.4.6-97.el7.centos
$ sudo systemctl enable httpd --now
Created symlink from /etc/systemd/system/multi-user.target.wants/httpd.service to /usr/lib/systemd/system/httpd.service.
```

```bash
$ sudo firewall-cmd --add-port=80/tcp --permanent
success
$ sudo firewall-cmd --reload
success
```
```bash
$ sudo yum install php
Installed:
php.x86_64 0:5.4.16-48.el7
$ sudo systemctl restart httpd
```

```bash
$ sudo yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
$ sudo yum install -y yum-utils
Loaded plugins: fastestmirror
$ sudo yum remove -y php
$ sudo yum-config-manager --enable remi-php56
$ sudo yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo -y
Loaded plugins: fastestmicurl -SLO https://wordpress.org/latest.tar.gz
$ curl -SLO https://wordpress.org/latest.tar.gz
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
 66 14.7M   66  9.8M    0     0   406k      0  0:00:37  0:00:24  0:00:13  218k

$ sudo tar xvzf wordpress.tar.gz tar xvzf wordpress.tar.gz
wordpress/wp-comments-post.php

$ sudo mv wordpress /var/www/html/wordpress
cp /var/www/html/wordpress/wp-config-sample.php /var/www/html/wordpress/wp-config.php

$ sudo nano /var/www/html/wordpress/wp-config.php
$ sudo nano /etc/httpd/conf/httpd.conf
/var/www/html/wordpress
apachectl configtest
Syntax OK


$ sudo systemctl reload httpd
```









































