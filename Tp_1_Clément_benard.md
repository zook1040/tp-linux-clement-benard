TP1

Je n'avais pas copier les résultats pour le grand I du  TP 1. 

# I. Utilisateurs

## 1. Création et configuration
#""    1. Creation et configuration

```bash
$ sudo useradd clement -m -s /bin/bash
$ sudo groupadd admin
$ sudo usermod -aG admin clement
```

## 2. SSH

Il faut mettre cette commande en powershell sur son pc :

 ssh-keygen -t rsa -b 4096
 
 A effectuer sur le serveur :
```bash
$ su admin_benz
$ sudo mkdir .ssh
$ sudo vim authorized_keys
$ sudo cat /home/admin_benz/.ssh/authorized_keys
ssh-rsa [...]
$ sudo chmod 600 /home/admin_benz/.ssh/authorized_keys
$ sudo chmod 700 /home/admin_benz/.ssh
```

# II. Configuration réseau

## 1. Nom de domaine

```bash
$ sudo hostname clement.lab
$ hostname
clement.lab
```

## 2. Serveur DNS

```bash
$ sudo vim /etc/sysconfig/network-scripts/ifcfg-ens33
cat /etc/sysconfig/network-scripts/ifcfg-ens33
BOOTPROTO=static
NAME=ens33
DEVICE=ens33
IPADDR=192.168.199.10
NETMASK=255.255.255.0
DNS1=1.1.1.1
ONBOOT=yes
```

# III. Partitionnement

## 1. Préparation de la VM

Mettre 3 disque dur de 3 Go. 

## 2. Partitionnement

```bash
$ sudo pvcreate /dev/sdb

$ sudo pvcreate /dev/sdcsudo vgcreate data_raid /dev/sdb
 Volume group "data_raid" successfully created
 
$ sudo vgextend data_raid /dev/sdc
Volume group "data_raid" successfully extended

$ sudo lvcreate -L 2G data_raid -n ma_lv_data
 Logical volume "ma_lv_data" created
 
$ sudo lvcreate -L 2G data_raid -n ma_lv_data2
Logical volume "ma_lv_data2" created

$ sudo lvcreate -l 100%FREE data_raid -n ma_lv_data3
Logical volume "ma_lv_data3" created
  root        centos    -wi-ao---- <17.00g
  swap        centos    -wi-ao----   2.00g
  ma_lv_data  data_raid -wi-a-----   2.00g
  ma_lv_data2 data_raid -wi-a-----   2.00g
  ma_lv_data3 data_raid -wi-a-----   1.99g

mkfs -t ext4 /dev/data_raid/ma_lv_data
mkfs -t ext4 /dev/data_raid/ma_lv_data2
mkfs -t ext4 /dev/data_raid/ma_lv_data3
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done

$ sudo mkdir /mnt/part1
$ sudo mkdir /mnt/part2
$ sudo mkdir /mnt/part3

$ mount /dev/data/ma_lv_data /mnt/part1
$ mount /dev/data_raid/ma_lv_data2 /mnt/part2
$ mount /dev/data_raid/ma_lv_data3 /mnt/part3

$ sudo vim /etc/fstab
/dev/data_raid/ma_lv_data /mnt/part1 ext4 defaults 0 0
/dev/data_raid/ma_lv_data2 /mnt/part2 ext4 defaults 0 0
/dev/data_raid/ma_lv_data3 /mnt/part3 ext4 defaults 0 0
```

# IV. Gestion de services

## 1. Interaction avec un service existant

```bash
$ sudo systemctl is-active firewalld
active
$ sudo systemctl is-enabled firewalld
enabled
```
## 2. Création de service

```bash
$ sudo sudo vim web.service 
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python2 -m SimpleHTTPServer 8888

[Install]
WantedBy=multi-user.target

$ sudo systemctl daemon-reload
$ sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
$ sudo systemctl start web

$ sudo firewall-cmd --add-port=8888/tcp --permanent
success

$ sudo firewall-cmd --list-all
public (active)
  ports: 8888/tcp
useradd web
mkdir /srv/lolo
sudo vim web.service etc/systemd/system
User=web
WorkingDirectory=/srv/lolo

$ sudo systemctl daemon-reload
$ sudo vim /srv/lolo
$ sudo cd /srv/lolo
$ sudo touch de.txt
```

























